# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [1.4.0](https://gitlab.com/tmorin/imzer/compare/v1.3.0...v1.4.0) (2019-04-25)


### Bug Fixes

* **ui:** increase opacity for the flash messages ([be5a917](https://gitlab.com/tmorin/imzer/commit/be5a917))
* **ui:** localization was disabled after first error ([87708c1](https://gitlab.com/tmorin/imzer/commit/87708c1))


### Features

* **ui:** add the imzer version ([7fd5fb2](https://gitlab.com/tmorin/imzer/commit/7fd5fb2))



# [1.3.0](https://gitlab.com/tmorin/imzer/compare/v1.2.5...v1.3.0) (2019-04-01)


### Features

* **PWA:** add app cache ([68e4460](https://gitlab.com/tmorin/imzer/commit/68e4460))



## [1.2.5](https://gitlab.com/tmorin/imzer/compare/v1.2.4...v1.2.5) (2019-04-01)


### Bug Fixes

* publishing ([73b30d9](https://gitlab.com/tmorin/imzer/commit/73b30d9))



## [1.2.4](https://gitlab.com/tmorin/imzer/compare/v1.2.3...v1.2.4) (2019-04-01)


### Bug Fixes

* website is not deployed ([3de3595](https://gitlab.com/tmorin/imzer/commit/3de3595))



## [1.2.3](https://gitlab.com/tmorin/imzer/compare/v1.2.2...v1.2.3) (2019-04-01)


### Bug Fixes

* auto geo localization should not be disabled when error is localization failed ([3c01b0e](https://gitlab.com/tmorin/imzer/commit/3c01b0e))



<a name="1.2.2"></a>
## [1.2.2](https://gitlab.com/tmorin/imzer/compare/v1.2.1...v1.2.2) (2018-10-25)


### Bug Fixes

* remove the cache feature ([c29efdd](https://gitlab.com/tmorin/imzer/commit/c29efdd))



<a name="1.2.1"></a>
## [1.2.1](https://gitlab.com/tmorin/imzer/compare/v1.2.0...v1.2.1) (2018-10-25)


### Bug Fixes

* the web app is not updated ([1a1f814](https://gitlab.com/tmorin/imzer/commit/1a1f814))



<a name="1.2.0"></a>
# 1.2.0 (2018-10-25)


### Bug Fixes

* add attributions ([23ef435](https://gitlab.com/tmorin/imzer/commit/23ef435))


### Features

* add label on hover to controls ([3ae1beb](https://gitlab.com/tmorin/imzer/commit/3ae1beb))
* add web-ext support ([1d6a09b](https://gitlab.com/tmorin/imzer/commit/1d6a09b))
* add web-ext support ([3203ecd](https://gitlab.com/tmorin/imzer/commit/3203ecd))
* add webmanifest support ([1ab9889](https://gitlab.com/tmorin/imzer/commit/1ab9889))
